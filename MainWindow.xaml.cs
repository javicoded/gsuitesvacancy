﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GsuitesVacancy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<User> users = new List<User>();
            users.Add(new User() { Reservation = "GF54D5", Unit = "123dd123", From = "John Doe", To = "21", Vacancy = "423466" });
            users.Add(new User() { Reservation = "GF54D5", Unit = "123dd123", From = "John Doe", To = "21", Vacancy = "423466" });
            users.Add(new User() { Reservation = "GF54D5", Unit = "123dd123", From = "John Doe", To = "21", Vacancy = "423466" });
            users.Add(new User() { Reservation = "GF54D5", Unit = "123dd123", From = "John Doe", To = "21", Vacancy = "423466" });

            dg.ItemsSource = users;

        }
        public class User
        {

            public string Reservation { get; set; }
            public string Unit { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string Vacancy { get; set; }
        }
    }
}
